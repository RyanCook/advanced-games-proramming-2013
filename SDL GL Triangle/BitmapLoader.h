#pragma once
#include "rt3d.h"
#include "rt3dObjLoader.h"
class BitmapLoader
{
public:
	BitmapLoader(void);
	~BitmapLoader(void);
	GLuint loadBitmap(char *fname);
};

