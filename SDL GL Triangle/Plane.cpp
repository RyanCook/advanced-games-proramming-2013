#include "Plane.h"

Plane::Plane() { }

Plane::~Plane() { }


void Plane::set3Points(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2)
{
	point0 = p0; point1 = p1; point2 = p2;
	
	glm::vec3 v, u;
	
	v = point1 - point0;
	u = point2 - point0;

	normal = glm::cross(v, u);

	normal = glm::normalize(normal);

	vectorDistance = -( glm::dot(normal, point0) );
}

float Plane::distance(glm::vec3 point)
{
	return ( vectorDistance + glm::dot( normal, point ) );
}

