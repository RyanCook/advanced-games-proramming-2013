#pragma once
#include "rt3d.h"
#include "ShaderProgram.h"
#include <string>





class ShaderManager
{
public:

	struct lightStruct {
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position[4];
};

struct materialStruct {
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	const GLfloat shininess;
};

	ShaderManager(void);
	~ShaderManager(void);
	 void initVertShaderProgram(const std::string name,const std::string vName,const std::string fName,const std::string vPath,const std::string fPath);
	void createShaderProgram(std::string _name);
	void attatchShader(std::string _name, ShaderType _type);
	void attatchShaderToProgram(std::string _Program, std::string _Shader);
	GLuint getProgramID(std::string _name);
	void compileShader(std::string _name);
	void linkProgramObject(std::string _name);
	void use(std::string _name);
	void bindAttribute(std::string _programName, GLuint index, std::string _attribName);
	void loadShaderSource(std::string _shaderName, std::string _sourceFile);
	ShaderProgram* operator[](const std::string &_programName);
	void setLight(std::string _program, const lightStruct light);
	void setMaterial(std::string _program, const materialStruct material);
private:
		std::map<std::string, ShaderProgram*> m_ShaderPrograms;
		std::map<std::string, Shader*> m_Shaders;
		bool m_DebugState;
		ShaderProgram* m_NullProgram;

};

