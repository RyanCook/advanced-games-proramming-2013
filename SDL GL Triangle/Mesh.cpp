#include "Mesh.h"
#include<map>

static std::map<GLuint, GLuint *> vertexArrayMap;

Mesh::Mesh(void)
{
}


Mesh::~Mesh(void)
{
}

/*
* - Code written by Daniel Livingstone.

* - Creates an entire mesh that is to be drawn on screen. Drawing to be taken care of in another function. Param names are pretty self-explanatory.

* - @param GLuint numVerts is the number of vertices the mesh that is being drawn has.

* - @param GLfloat* vertices is the vertex points.

* - @param GLfloat* colours is the colour of the vertices that are to be drawn.

* - @param GLfloat* normals is used to pass in the normals of the vertices.

* - @param GLfloat* texcoords is used to pass in the texture co-ords for the vertices.

* - @param GLuint indexCount is the number of vertices that are being passed in.

* - @param GLuint indicies is the index value belonging to each vertex.
*/
GLuint Mesh::createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
	const GLfloat* texcoords, const GLfloat* tangents, const GLfloat* bitangents, const GLuint indexCount, const GLuint* indices)
{
	GLuint VAO;
	// generate and set up a Vertex Array Object for the mesh
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	//an array to store each Vertex Buffer Object.
	GLuint *pMeshBuffers = new GLuint[8];

	//Checks if at least vertices are passed in as you cannot create a mesh without vertices.
	if (vertices == nullptr) {

		std::cerr << "Cannot create a mesh without vertices!!";
		return 0;
	}

	// generate and set up the Vertex Buffer Objects for the data
	GLuint VBO;
	glGenBuffers(1, &VBO);

	// VBO for vertex data
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	/* set the size of the buffer by multiplying the number of vertices by 3 as each vertex has 3 points. 
	These points are GLfloats, that is why it is finally multiplied by the sizeof a GLfloat. 
	Then it pases in the vertices. This format remains the same for the rest of the attributes.
	*/
	glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)MESHVERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(MESHVERTEX);
	pMeshBuffers[MESHVERTEX] = VBO;


	// VBO for colour data
	if (colours != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), colours, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)COLOUR, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(COLOUR);
		pMeshBuffers[COLOUR] = VBO;
	}

	// VBO for normal data
	if (normals != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), normals, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(NORMAL);
		pMeshBuffers[NORMAL] = VBO;
	}

	// VBO for tex-coord data
	if (texcoords != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 2*numVerts*sizeof(GLfloat), texcoords, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(TEXCOORD);
		pMeshBuffers[TEXCOORD] = VBO;
	}

	// VBO for tangent data
	if (tangents != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), tangents, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)TANGENT, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(TANGENT);
		pMeshBuffers[TANGENT] = VBO;
	}

	// VBO for bitangent data
	if (bitangents != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), bitangents, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)BITANGENT, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(BITANGENT);
		pMeshBuffers[BITANGENT] = VBO;
	}

	if (indices != nullptr && indexCount > 0) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(GLuint), indices, GL_STATIC_DRAW);
		pMeshBuffers[INDEX] = VBO;
	}
	// unbind vertex array
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	// return the identifier needed to draw this mesh

	vertexArrayMap.insert( std::pair<GLuint, GLuint *>(VAO, pMeshBuffers) );

	return VAO;
}


/*
* - Allows created mesh's to be drawn. However this function only draws one primitive, no indexed mesh's

* - @param GLuint mesh is the mesh that is created and is now passed in to the draw function.

* - @param GLuint numVerts is the number of vertices the mesh has.

* - @param GLuint primitive is the type of primitive that is to be drawn.
*/
void Mesh::drawMesh(const GLuint mesh, const GLuint numVerts, const GLuint primitive) {
	glBindVertexArray(mesh);	// Bind mesh VAO
	glDrawArrays(primitive, 0, numVerts);	// draw first vertex array object
	glBindVertexArray(0);
}

/*
* - This draw function allows mesh's that are made up of multiple primitives to be drawn.

* - The only param in this function that serves a different purpose to the ones above is

* - @param GLuint indexCount as this allows vertices to be shared between primitives
*/
void Mesh::drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive) {
	glBindVertexArray(mesh);	// Bind mesh VAO
	glDrawElements(primitive, indexCount,  GL_UNSIGNED_INT, 0);	// draw VAO 
	glBindVertexArray(0);
}

/*
This allows a mesh to animate.
*/
void Mesh::updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size) {
	GLuint * pMeshBuffers = vertexArrayMap[mesh];
	glBindVertexArray(mesh);

	// Delete the old buffer data
	glDeleteBuffers(1, &pMeshBuffers[bufferType]);

	// generate and set up the VBOs for the new data
	GLuint VBO;
	glGenBuffers(1, &VBO);
	// VBO for the data
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, size*sizeof(GLfloat), data, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)bufferType, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(bufferType);
	pMeshBuffers[MESHVERTEX] = VBO;

	glBindVertexArray(0);

}

//PART OF TUTORIAL

glm::vec2 Mesh::FloatToVec2( std::vector<GLfloat> vec2, int counter )
{
	glm::vec2 tmp;
	tmp.x = vec2[counter+0];
	tmp.y = vec2[counter+1];
	return tmp;
}


glm::vec3 Mesh::FloatToVec3( std::vector<GLfloat> vec3, int counter )
{
	glm::vec3 tmp;
	tmp.x = vec3[counter+0];
	tmp.y = vec3[counter+1];
	tmp.z = vec3[counter+2];
	return tmp;
}

glm::vec3 Mesh::FloatToVec3Norm(std::vector<GLfloat> vec3Norm, int counter)
{
	glm::vec3 tmp;
	tmp.x = vec3Norm[counter+0];
	tmp.y = vec3Norm[counter+1];
	tmp.z = vec3Norm[counter+2];
	return tmp;
}


void Mesh::computeTangentBasis(std::vector<GLfloat> vertices, std::vector<GLfloat> texCoords, std::vector<GLfloat> normals, 
	std::vector<GLfloat> tangents, std::vector<GLfloat> bitangents, GLuint size, std::vector<GLuint> indicies )
{
	std::vector<glm::vec3> vertex;
	std::vector<glm::vec2> texCoordinates;
	std::vector<glm::vec3> normal;
	std::vector<glm::vec3> Tangents;
	std::vector<glm::vec3> Bitangent;

	int vec3Counter = 0;
	int vec2Counter = 0;
	//This for loops changes the vectors from GLfloats to vec3's to be used in the calculations below
	for(int i=0; i<size; i++)
	{
		vertex.push_back(FloatToVec3(vertices, vec3Counter));

		texCoordinates.push_back( FloatToVec2( texCoords, vec2Counter ) );

		vec3Counter+=3;
		vec2Counter+=2;

	}
	int normCount = 0;
	GLuint size1 = normals.size();
	for(int i=0; i<size1/3; i++)
	{

		normal.push_back( FloatToVec3Norm( normals, normCount ) );

		normCount+=3;

	}

	for (int i=0; i<indicies.size(); i+=3)
	{
		//shortcut to vertices
		glm::vec3 v0 = vertex[indicies[i+0]];
		glm::vec3 v1 = vertex[indicies[i+1]];
		glm::vec3 v2 = vertex[indicies[i+2]];

		//shortcut to texcoords
		glm::vec2 texCoord0 = texCoordinates[i+0];
		glm::vec2 texCoord1 = texCoordinates[i+1];
		glm::vec2 texCoord2 = texCoordinates[i+2];

		// Edges of the triangle : postion delta
		glm::vec3 deltaPos1 = v1-v0;
		glm::vec3 deltaPos2 = v2-v0;

		// Tex coord delta
		glm::vec2 deltaTexCoord1 = texCoord1-texCoord0;
		glm::vec2 deltaTexCoord2 = texCoord2-texCoord0;

		float r = 1.0f / (deltaTexCoord1.x * deltaTexCoord2.y - deltaTexCoord1.y * deltaTexCoord2.x);


		glm::vec3 tangent, bitangent;


		//now calculate our tangents and bitangents.
		glm::vec3 Tangent, BiTangent;
		Tangent.x = r * (deltaTexCoord2.y * deltaPos1.x - deltaTexCoord1.x * deltaPos2.x);
		Tangent.y = r * (deltaTexCoord2.y * deltaPos1.y - deltaTexCoord1.x * deltaPos2.y);
		Tangent.z = r * (deltaTexCoord2.y * deltaPos1.z - deltaTexCoord1.x * deltaPos2.z);

		BiTangent.x = (-deltaTexCoord2.x * deltaPos1.x + deltaTexCoord1.x * deltaPos2.x) * r;
		BiTangent.y = (-deltaTexCoord2.x * deltaPos1.y + deltaTexCoord1.x * deltaPos2.y) * r;
		BiTangent.z = (-deltaTexCoord2.x * deltaPos1.x + deltaTexCoord1.x * deltaPos2.x) * r;

		//Tangent = glm::normalize(Tangent - normal[i] * glm::dot(normal[i], Tangent));


		Bitangent.push_back(BiTangent);
		Bitangent.push_back(BiTangent);
		Bitangent.push_back(BiTangent);

		Tangents.push_back(Tangent);
		Tangents.push_back(Tangent);
		Tangents.push_back(Tangent);

	}


	//	//calculating handedness.
	//if (dot(cross(in_Normal, in_Tangent), in_Bitangent) < 0.0f){
 //       in_Tangent = in_Tangent * -1.0f;
 //   }


	for (int i = 0; i < normal.size(); i++)
	{
		
		glm::vec3 biTangent = glm::cross(normal[i],Tangents[i]);

		if(glm::dot(biTangent, Bitangent[i]) <0.0f)
		{
			Tangents[i]*=-1.0f;
		}
	}


	for(int i = 0; i < Tangents.size();i++)
	{
		//Changes the vec3's back into a vector of GLfloats for them to be used by openGL
		tangents.push_back(Tangents[i].x);
		tangents.push_back(Tangents[i].y);
		tangents.push_back(Tangents[i].z);
		bitangents.push_back(Bitangent[i].x);
		bitangents.push_back(Bitangent[i].y);
		bitangents.push_back(Bitangent[i].z);
	}



}