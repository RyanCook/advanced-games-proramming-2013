#pragma once
#include<glm\glm.hpp>
#include<GL\glew.h>
#include<ctime>

#define PARTICLEPOSITION 0
#define PARTICLECOLOUR	 1 
#define PARTICLEVELOCITY 2
#define STARTTIME		 3

class Particles
{
public:
	Particles(const int n);
	~Particles(void);
	GLfloat *getColours() { return colours;}
	GLfloat *getVelocitys() { return velocity;}
	GLfloat *getPositions() { return positions;}
	int getNumOfParticles() { return numberOfParticles;}
	void Update();
	void Init(GLuint shaderProgram);
	void Draw();
private:
	GLfloat *colours;
	GLfloat *positions;
	GLfloat *velocity;
	GLfloat *startTime;
	GLfloat *Transparency;
	GLfloat *lifetime;
	GLfloat *size;
	GLuint *pMeshBuffers;
	int numberOfParticles;
	GLuint VAO;
	GLuint VBO;
};

