#version 330

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};


//incoming surface normal and view-space position from vert shader
in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec3 lightDistance;
in vec3 ex_Reflection;
in vec2 ex_TexCoord;
in vec3 ex_WorldNorm;
in vec3 ex_WorldView;


//final fragment colour
layout (location = 0) out vec4 colour;

uniform lightStruct light;
uniform materialStruct material;
//The cube map texture
uniform samplerCube cubeMapTex;
//use the texture from texture unit 2
uniform sampler2D texture2;

void main(void)
{

	float actualLightDistance = length(lightDistance);
	// Ambient intensity
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot((ex_N),(ex_L)),0);

	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize( reflect( (-ex_L),(ex_N) ) );

	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow( max (dot( R, ex_V ),0 ), material.shininess );

	vec3 finalColour = vec3(ambientI + diffuseI + specularI) * (1/(1 + 0.001*actualLightDistance + 0.001*actualLightDistance*actualLightDistance));


	//Calculate the texture co-ord by reflecting the
	//view-space position around the surface normal
	//should possible be done in the vertex shader?
	// -ve normalized vertex position ex_V results in right side up, however transparent mirrored images.

	vec3 reflectedTextureCoord = reflect(-ex_WorldView, normalize( ex_WorldNorm ) );

	//Sample the texture and colour the resulting fragment A colour of the light colour and texture mixed together.
	//Why does including the - texture(texture2, ex_TexCoord) - affect the bunny also???? 
	//Why do you multiply the second part but add the first????
	colour = vec4(finalColour, 1.0) + texture( texture2, ex_TexCoord ) * texture( cubeMapTex, reflectedTextureCoord );
	//colour = texture( cubeMapTex, reflectedTextureCoord );
	//colour = vec4(reflectedTextureCoord, 1.0);
}