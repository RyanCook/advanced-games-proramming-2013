// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;

in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec3 lightDistance;
layout(location = 0) out vec4 out_colour;
 
void main(void) {
    
	float actualLightDistance = length(lightDistance);
	// Ambient intensity
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot((ex_N),(ex_L)),0);

	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(reflect((-ex_L),(ex_N)));

	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);

	vec3 finalColour = vec3(ambientI + diffuseI + specularI) * ( 1 / (1 + 0.001*actualLightDistance + 0.001*actualLightDistance*actualLightDistance) );




	vec4 litColour = vec4(finalColour, 1.0);
	vec4 shade1 = smoothstep(vec4(0.2),vec4(0.21),litColour);
	vec4 shade2 = smoothstep(vec4(0.4),vec4(0.41),litColour);
	vec4 shade3 = smoothstep(vec4(0.8),vec4(0.81),litColour);
	vec4 colour = max( max(0.3*shade1,0.5*shade2), shade3 );

	if ( abs(dot(ex_N,ex_V)) < 0.5)
		colour = vec4(vec3(0.0),1.0);
		out_colour = colour;


	// Fragment colour
	//out_Color = vec4(finalColour, 1.0));
	//out_Color = texture2D(textureUnit0, ex_TexCoord);


}