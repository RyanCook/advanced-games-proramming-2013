#pragma once
#include"BitmapLoader.h"
#include "rt3d.h"
#include"Mesh.h"
#include"Particles.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include"Model.h"
#include"Frustum.h"




using namespace std;

class NewGame
{
public:
	NewGame(void);
	~NewGame(void);
	SDL_Window * setupRC(SDL_GLContext &context);
	GLuint loadCubeMap(const char *fname[6], GLuint *texID);
	void init(void);
	glm::vec3 moveForward(glm::vec3 pos, GLfloat angle, GLfloat d, GLfloat e);
	glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d);
	void update(void);
	void GLSLInit();
	void draw(SDL_Window *window);
	void run();

private:

	GLuint meshIndexCount;
	GLuint md2VertCount;
	GLuint roundWallIndexCount;
	GLuint meshObjects[3];

	GLuint shaderProgram;
	GLuint bumpMapProgram;
	GLuint skyboxProgram;
	GLuint bumpMapTangentProgram;
	GLuint particleShaderProgram;


	GLfloat r;
	GLfloat e;
	GLfloat theta;

	stack<glm::mat4> mvStack; 
	// TEXTURE STUFF
	GLuint textures[6];
	GLuint skybox[5];
	GLuint labels[5];
	GLfloat Timer;

	clock_t timer;

	BitmapLoader* LoadBitmapObj;
	Mesh* MeshObj;
	Particles* ParticleCreator;
	Model* cubeBumpModel;
	Frustum* frustumObj;
};

