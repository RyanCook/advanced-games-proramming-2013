#include "Frustum.h"

Frustum::Frustum() { }

Frustum::~Frustum() { }

void Frustum::setFrustrum(float fov, float ratio, float nearClip, float farClip)
{
	this->fov = fov;
	this->ratio = ratio;
	nearDistance = nearClip;
	farDistance = farClip;	

	// Calcuate Height and Width of the Near and Far Planes
	nearHeight = (float)tan(DEG_TO_RADIAN * this->fov * 0.5) * nearDistance;
	nearWidth = nearHeight * this->ratio;
	farHeight = (float)tan(DEG_TO_RADIAN * this->fov * 0.5) * farDistance;
	farWidth = farHeight * this->ratio;
}

void Frustum::setCamera(glm::vec3 eye, glm::vec3 at, glm::vec3 up)
{
	glm::vec3 direction, xAxis, yAxis, zAxis;

	zAxis = eye - at;
	zAxis = glm::normalize(zAxis);

	xAxis = glm::cross(up, zAxis);

	yAxis = glm::cross(zAxis, xAxis);

	nearCenter = eye - zAxis * nearDistance;
	farCenter = eye - zAxis * farDistance;

	nearTopLeft = nearCenter + yAxis * nearHeight - xAxis * nearWidth;
	nearTopRight = nearCenter + yAxis * nearHeight + xAxis * nearWidth;
	nearBottomLeft = nearCenter - yAxis * nearHeight - xAxis * nearWidth;
	nearBottomRight = nearCenter - yAxis * nearHeight + xAxis * nearWidth;

	farTopLeft = farCenter + yAxis * farHeight - xAxis * farWidth;
	farTopRight = farCenter + yAxis * farHeight + xAxis * farWidth;
	farBottomLeft = farCenter - yAxis * farHeight - xAxis * farWidth;
	farBottomRight = farCenter - yAxis * farHeight + xAxis * farWidth;

	planes[TOP].set3Points(nearTopRight ,nearTopLeft, farTopLeft);
	planes[BOTTOM].set3Points(nearBottomLeft, nearBottomRight, farBottomRight);
	planes[LEFT].set3Points(nearTopLeft, nearBottomLeft, farBottomLeft);
	planes[RIGHT].set3Points(nearBottomRight, nearTopRight, farBottomRight);
	planes[NEARP].set3Points(nearTopLeft, nearTopRight, nearBottomRight);
	planes[FARP].set3Points(farTopRight, farTopLeft, farBottomLeft);
}

int Frustum::sphereInFrustum(glm::vec3 center, float radius) {

	int result = INSIDE;
	float distance;

	for(int i=0; i < 6; i++) {
		distance = planes[i].distance(center);
		if (distance < -radius)
			return OUTSIDE;
		else if (distance < radius)
			result =  INTERSECT;
	}
	return(result);

}

int Frustum::cubeInFrustum(Model &model, glm::vec3 translation)
{
	int result = INSIDE, outFrustum, inFrustum;

	// Check each plane
	for(int i = 0; i < 6; i++)
	{
		// Resetet counters for inside the frustum and out the frustum
		outFrustum = 0; inFrustum = 0;

		for(int k = 0; k < model.getSize() && (inFrustum == 0 || outFrustum ==0); k++)
		{
			if(planes[i].distance(model.getVerts(k, translation)) < 0)
				outFrustum++;
			else
				inFrustum++;
		}
		if (!inFrustum)
			return (OUTSIDE);
		else if (outFrustum)
			result = INTERSECT;
	}
	return (result);
}

