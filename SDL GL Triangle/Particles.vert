// phong.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 330

uniform mat4 modelview, projection;
uniform float timer;
uniform vec3 particleEmitter;


//layout (location = 0) in vec3 in_particlePosition;
in vec3 in_particleColour;
in vec3 in_particleVelocity;
in float in_startTime;

out vec4 ex_Colour;

void main(void){
	float alpha = 1.0f;
	float t = timer - in_startTime;
	// vertex into eye coordinates
	//vec4 pointPosition = modelview * vec4(in_particlePosition,1.0);
   // gl_Position = projection * pointPosition;
	vec3 position = particleEmitter;


	if (t >= 0 ) 
	{
	//gravity, hence -ve in y direction.
	vec3 acceleration = vec3(0.0, -0.001, 0.0);
	//standard equation of motion
	position += in_particleVelocity * t + 0.5 * acceleration * t * t;
	}

	//while (position.y < 0.0) position.y += particleEmitter.y;

	if(position.y < 0.0)
	{
		alpha = alpha - (t/3.0);

		if(alpha < 0.1)
		{
		position = particleEmitter ;
		alpha = 1.0;
		}
	}
	vec4 pointPosition = modelview * vec4( position, 1.0 );
	gl_Position = projection * pointPosition;
	ex_Colour = vec4(in_particleColour, alpha);
}