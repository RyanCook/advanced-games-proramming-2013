#version 330

uniform sampler2D particleTexture;

in vec4 ex_Colour;

out vec4 out_Colour;



void main(void){
	//out_Colour = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	out_Colour = ex_Colour * texture2D(particleTexture, gl_PointCoord);

}