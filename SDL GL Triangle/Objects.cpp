#include "Objects.h"


Objects::Objects(void)
{
	BuildMesh = new Mesh();
	meshIndexCount = 0;
}


Objects::~Objects(void)
{

}


void Objects::Draw()
{

}
	
void Objects::Init(std::string fname)
{
	BuildMesh = new Mesh();
	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLuint> indices;
	std::vector<GLfloat> tangents;
	std::vector<GLfloat> bitangents;

	rt3d::loadObj(fname.c_str(), verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	meshIndexCount = size;
	BuildMesh->computeTangentBasis(verts, norms, tex_coords, tangents, bitangents, size, indices);
	mesh = BuildMesh->createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), tangents.data(), bitangents.data(), size, indices.data());
	
}

GLuint Objects::getMesh()
{
	return mesh;
}