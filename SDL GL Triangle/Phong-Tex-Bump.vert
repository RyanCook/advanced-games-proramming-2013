// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 330

uniform mat4 modelview;
uniform mat4 projection;
uniform vec4 lightPosition;
uniform vec3 cameraPos;
//uniform mat3 normalmatrix;

in vec3 in_Position;
in vec3 in_Tangent;
in vec3 in_Bitangent;
in vec3 in_Normal;
out vec3 ex_V;
out vec3 ex_L;
out vec3 lightDistance;
out vec3 lightDirection_TangentSpace;
out vec3 cameraDirection_TangentSpace;
in vec2 in_TexCoord;
out vec2 ex_TexCoord;


// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void) {
	//TBN matrix stuff
	mat3 modelView3x3 = mat3(modelview);

	//Convert to camera space as it is easier to work with fragments here.
	vec3 vertexNormal_CameraSpace = modelView3x3 * normalize(in_Normal);
	vec3 vertexTangent_CameraSpace = modelView3x3 * normalize( ( in_Tangent - in_Normal ) * dot( in_Normal, in_Tangent ) );
	vec3 Bitangent = normalize( cross( vertexNormal_CameraSpace, vertexTangent_CameraSpace ) );

	//This is the TBN matrix which allows vectors to be changed into tangent space.
	mat3 TBN = transpose(mat3(
		vertexTangent_CameraSpace,
		Bitangent,
		vertexNormal_CameraSpace
		));

	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);

	//// Find V - in eye coordinates, eye is at (0,0,0)
	//ex_V = normalize(-vertexPosition).xyz;



	// L - to light source from vertex
	lightDistance = lightPosition.xyz - vertexPosition.xyz;
	ex_L = normalize(lightPosition.xyz - vertexPosition.xyz);
	vec3 lightDirection_CameraSpace = modelView3x3 * ex_L;

	ex_V = TBN * vec3(vertexPosition);

	//find camera direction
	vec3 cameraDirection = normalize(cameraPos.xyz - vertexPosition.xyz);
	vec3 cameraDirection_CameraSpace = modelView3x3 * cameraDirection;

	//camera and light direction in tangent space
	lightDirection_TangentSpace = TBN * lightDirection_CameraSpace;
	cameraDirection_TangentSpace = TBN * cameraDirection_CameraSpace;



	ex_TexCoord = in_TexCoord;

    gl_Position = projection * vertexPosition;

}