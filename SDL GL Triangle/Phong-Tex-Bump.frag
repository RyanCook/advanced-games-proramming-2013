// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D ColourMap;
uniform sampler2D NormalMap;

in vec3 ex_V;
in vec3 ex_L;
in vec3 lightDistance;
in vec3 lightDirection_TangentSpace;
in vec3 cameraDirection_TangentSpace;
in vec2 ex_TexCoord;
layout(location = 0) out vec4 out_Color;
 
void main(void) {
    
	//extracts the normals from the normal map texture
	vec3 normal = normalize(texture2D(NormalMap, ex_TexCoord).rgb * 2.0 - 1.0);

	float actualLightDistance = length(lightDistance);
	// Ambient intensity
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;

	diffuseI = diffuseI * max(dot(normal, lightDirection_TangentSpace), 0.0);

	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(reflect((-lightDirection_TangentSpace),(normal)));

	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(cameraDirection_TangentSpace, R),0), material.shininess);

	//vec3 finalColour = vec3(ambientI + diffuseI + specularI) * texture2D(ColourMap, ex_TexCoord).rgb * (1/(1 + 0.001*actualLightDistance + 0.001*actualLightDistance*actualLightDistance));
	// Fragment colour
	vec3 finalColour =  vec3(diffuseI);
	out_Color = vec4(finalColour, 1.0);
	//out_Color = vec4(1.0,1.0,1.0,1.0);

}