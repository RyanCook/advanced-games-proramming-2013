#ifndef PLANE_H
#define PLANE_H

#include <glm\glm.hpp>
#include <math.h>

#include "Model.h"

class Plane
{
public:
	Plane();
	~Plane();


	void set3Points(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2);

	float distance(glm::vec3 point);

	glm::vec3 getNormal() { return normal; }

private:
	float vectorDistance;
	
	glm::vec3 point0;
	glm::vec3 point1;
	glm::vec3 point2;

	glm::vec3 normal;

};

#endif