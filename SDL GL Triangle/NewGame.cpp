#include "NewGame.h"


#define DEG_TO_RADIAN 0.017453293


glm::vec3 eye(0.0f, 1.0f, 0.0f);
glm::vec3 at(0.0f, 1.0f, -1.0f);
glm::vec3 up(0.0f, 1.0f, 0.0f);


GLfloat lightX = -10.0f;
GLfloat lightY = 10.0f;
GLfloat lightZ = 10.0f;
glm::vec4 lightPos(lightX, lightY, lightZ, 1.0f); //light position

rt3d::lightStruct light0 = {
	{1.0f, 1.0f, 1.0f, 1.0f}, // ambient
	{1.0f, 1.0f, 1.0f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{-10.0f, 10.0f, 10.0f, 1.0f}  // position
}; 




rt3d::materialStruct material0 = {
	{0.2f, 0.4f, 0.2f, 1.0f}, // ambient
	{0.5f, 1.0f, 0.5f, 1.0f}, // diffuse
	{0.0f, 0.1f, 0.0f, 1.0f}, // specular
	1.0f  // shininess
};
rt3d::materialStruct material1 = {
	{0.5f, 0.5f, 0.5f, 0.5f}, // ambient
	{0.5f, 0.5f, 0.5f, 0.5f}, // diffuse
	{0.5f, 0.5f, 0.5f, 0.5f}, // specular
	1.0f  // shininess
};

NewGame::NewGame(void)
{
	Timer = 0;
	meshIndexCount = 0;
	r = 0.0f;
	e = 0.0f;
	theta = 0.0f;
	ParticleCreator = new Particles(50000);
}




NewGame::~NewGame(void)
{
}




SDL_Window *NewGame::setupRC(SDL_GLContext &context)
{
	SDL_Window * window;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
		rt3d::exitFatalError("Unable to initialize SDL"); 

	// Request an OpenGL 3.0 context.

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)

	// Create 800x600 window
	window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
		rt3d::exitFatalError("Unable to create window");

	context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
	SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}



GLuint NewGame::loadCubeMap(const char *fname[6], GLuint *texID)
{
	glGenTextures(1, texID); // generate texture ID
	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y };
	SDL_Surface *tmpSurface;
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind texture and set parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, 
		GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, 
		GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, 
		GL_CLAMP_TO_EDGE);
	for (int i=0;i<6;i++)
	{
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
		if (!tmpSurface)
		{
			std::cout << "Error loading bitmap" << std::endl;
			return *texID;
		}
		glTexImage2D(sides[i],0,GL_RGB,tmpSurface->w, tmpSurface->h, 0,
			GL_BGR, GL_UNSIGNED_BYTE, 
			tmpSurface->pixels);
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}
	return *texID; // return value of texure ID, redundant really
}



void NewGame::init(void)
{
	LoadBitmapObj = new BitmapLoader();
	MeshObj = new Mesh();
	cubeBumpModel = new Model();
	frustumObj = new Frustum();

	textures[0] = LoadBitmapObj->loadBitmap("BricksColour.bmp");
	textures[1] = LoadBitmapObj->loadBitmap("BricksNormals.bmp");
	textures[2] = LoadBitmapObj->loadBitmap("smoke1.bmp");
	textures[3] = LoadBitmapObj->loadBitmap("snow1.bmp");
	textures[4] = LoadBitmapObj->loadBitmap("snowNormal.bmp");

	shaderProgram = rt3d::initShaders("oldphong-tex.vert","oldphong-tex.frag", false);
	rt3d::setLight( shaderProgram, light0 );
	rt3d::setMaterial( shaderProgram, material0 );

	bumpMapProgram = rt3d::initShaders( "Phong-Tex-Parallax.vert","Phong-Tex-Parallax.frag", false );
	rt3d::setLight( bumpMapProgram, light0 );
	rt3d::setMaterial( bumpMapProgram, material0 );
	GLuint loc = glGetUniformLocation( bumpMapProgram, "ColourMap" );
	glUniform1i(loc, 0);
	loc = glGetUniformLocation( bumpMapProgram, "NormalMap" );
	glUniform1i(loc, 1);


	bumpMapTangentProgram = rt3d::initShaders( "Phong-Tex-Bump.vert","Phong-Tex-Bump.frag", false );
	glUseProgram(bumpMapTangentProgram);
	rt3d::setLight( bumpMapTangentProgram, light0 );
	rt3d::setMaterial( bumpMapTangentProgram, material0 );
	loc = glGetUniformLocation( bumpMapTangentProgram, "ColourMap" );
	glUniform1i(loc, 0);
	loc = glGetUniformLocation( bumpMapTangentProgram, "NormalMap" );
	glUniform1i(loc, 1);


	skyboxProgram = rt3d::initShaders("cubeMap.vert","cubeMap.frag", false);

	particleShaderProgram = rt3d::initShaders("Particles.vert", "Particles.frag", true);
	loc = glGetUniformLocation( particleShaderProgram, "particleTexture" );
	glUniform1i(loc, 0);

	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	vector<GLfloat> tangents;
	vector<GLfloat> bitangents;

	rt3d::loadObj("cube.obj", verts, norms, tex_coords, indices);

	GLuint size = indices.size();
	meshIndexCount = size;
	MeshObj->computeTangentBasis(verts, norms, tex_coords, tangents, bitangents, size, indices);
	//meshObjects[0] = MeshObj->createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), tangents.data(), bitangents.data(), size, indices.data());
	cubeBumpModel->init(verts, norms, tex_coords, indices);

	ParticleCreator->Init(particleShaderProgram);


	//loading in the textures for the cube map

	const char *cubeTexFiles[6] = {
		"Snow2048_bk.bmp", "Snow2048_ft.bmp",
		"Snow2048_rt.bmp", "Snow2048_lf.bmp",
		"Snow2048_up.bmp", "Snow2048_dn.bmp"
	};

	loadCubeMap(cubeTexFiles, &skybox[0]);

	glEnable(GL_POINT_SPRITE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

}

glm::vec3 NewGame::moveForward(glm::vec3 pos, GLfloat angle, GLfloat d, GLfloat e)
{
	return glm::vec3(pos.x + d*std::sin(angle*DEG_TO_RADIAN), pos.y + d*std::tan(e*DEG_TO_RADIAN), pos.z - d*std::cos(angle*DEG_TO_RADIAN));
}

glm::vec3 NewGame::moveRight(glm::vec3 pos, GLfloat angle, GLfloat d) {
	return glm::vec3(pos.x + d*std::cos(angle*DEG_TO_RADIAN), pos.y, pos.z + d*std::sin(angle*DEG_TO_RADIAN));
}

void NewGame::update(void)
{
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_W] ) eye = moveForward(eye,r,0.1f, e);
	if ( keys[SDL_SCANCODE_S] ) eye = moveForward(eye,r,-0.1f, e);
	if ( keys[SDL_SCANCODE_A] ) eye = moveRight(eye,r,-0.1f);
	if ( keys[SDL_SCANCODE_D] ) eye = moveRight(eye,r,0.1f);
	if ( keys[SDL_SCANCODE_R] ) eye.y += 0.1;
	if ( keys[SDL_SCANCODE_F] ) eye.y -= 0.1;
	if ( keys[SDL_SCANCODE_Q] ) e += 1.2;
	if ( keys[SDL_SCANCODE_E] ) e -= 1.2;
	if ( keys[SDL_SCANCODE_UP] ) lightZ -= 1.0;
	if ( keys[SDL_SCANCODE_DOWN] ) lightZ += 1.1;
	if ( keys[SDL_SCANCODE_LEFT] ) lightX -= 1.1;
	if ( keys[SDL_SCANCODE_RIGHT] ) lightX += 1.1;
	if ( keys[SDL_SCANCODE_KP_2] ) lightY -= 1.1;
	if ( keys[SDL_SCANCODE_KP_8] ) lightY += 1.1;

	if ( keys[SDL_SCANCODE_COMMA] ) r -= 1.0f;
	if ( keys[SDL_SCANCODE_PERIOD] ) r += 1.0f;

	if ( keys[SDL_SCANCODE_1] ) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_CULL_FACE);
	}
	if ( keys[SDL_SCANCODE_2] ) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_CULL_FACE);
	}

	//ParticleCreator->Update();

}

void NewGame::draw(SDL_Window *window)
{
	// clear the screen
	glEnable(GL_CULL_FACE);
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,500.0f);
	frustumObj->setFrustrum(60.0f,800.0f/600,1.0f,150.0f);
	GLfloat scale(1.0f); // just to allow easy scaling of complete scene 
	glm::mat4 modelview(1.0); // set base position for scene

	mvStack.push(modelview);

	at = moveForward(eye,r,1.0f,e);
	mvStack.top() = glm::lookAt(eye,at,up);


	// draw a skybox
	// then render skybox as single cube using cube map
	glUseProgram(skyboxProgram);
	rt3d::setUniformMatrix4fv(skyboxProgram, "projection", glm::value_ptr(projection));
	glDepthMask(GL_FALSE); // make sure writing to update depth test is off
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	mvStack.push( glm::mat4( mvRotOnlyMat3 ) );

	glCullFace(GL_FRONT); // drawing inside of cube!
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox[0]);
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(1.5f, 1.5f, 1.5f));
	rt3d::setUniformMatrix4fv(skyboxProgram, "modelview", glm::value_ptr(mvStack.top()));
	cubeBumpModel->draw();
	mvStack.pop();
	glCullFace(GL_BACK);

	frustumObj->setCamera(eye, at, up);

	glDepthMask(GL_TRUE);


	glUseProgram( shaderProgram );

	rt3d::setUniformMatrix4fv( shaderProgram, "projection", glm::value_ptr( projection ) );

	//draw cube for light.
	glm::vec4 lightPos(lightX, lightY, lightZ, 1.0f); //light position
	glm::vec4 tmp = mvStack.top()*lightPos;
	light0.position[0] = tmp.x;
	light0.position[1] = tmp.y;
	light0.position[2] = tmp.z;
	rt3d::setLightPos( shaderProgram, glm::value_ptr( tmp ) );



	// draw a cube 
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	mvStack.push(mvStack.top());
	cubeBumpModel->move(glm::vec3 (5.0f, 5.0f, -5.0f));
	mvStack.top() = glm::translate(mvStack.top(), cubeBumpModel->getCenterPos());
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 2.0f, 2.0f));
	rt3d::setUniformMatrix4fv( shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setMaterial( shaderProgram, material1 );
	if ( ( frustumObj->cubeInFrustum( *cubeBumpModel, cubeBumpModel->getCenterPos() ) ) != Frustum::OUTSIDE) 
		cubeBumpModel->draw();
	mvStack.pop();

	// draw a cube round the light
	mvStack.push(mvStack.top());
	cubeBumpModel->move( glm::vec3(lightX, lightY, lightZ) );
	mvStack.top() = glm::translate(mvStack.top(), cubeBumpModel->getCenterPos());
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 2.0f, 2.0f));
	rt3d::setUniformMatrix4fv( shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setMaterial( shaderProgram, material1 );
	if ( ( frustumObj->cubeInFrustum( *cubeBumpModel, cubeBumpModel->getCenterPos() ) ) != Frustum::OUTSIDE) 
		cubeBumpModel->draw();
	mvStack.pop();

	// draw another cube for bump mapping.
	glUseProgram( bumpMapProgram );

	rt3d::setUniformMatrix4fv( bumpMapProgram, "projection", glm::value_ptr( projection ) );
	rt3d::setLightPos( bumpMapProgram, glm::value_ptr( tmp ) );

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[0]);

	glActiveTexture(GL_TEXTURE1);
	//bind the texture to this texture unit.
	glBindTexture(GL_TEXTURE_2D, textures[1]);

	mvStack.push(mvStack.top());
	cubeBumpModel->move( glm::vec3( -2.0f, 5.0f, -5.0f ) );
	mvStack.top() = glm::translate(mvStack.top(),cubeBumpModel->getCenterPos());
	mvStack.top() = glm::rotate( mvStack.top(), 45.0f, glm::vec3( 1.0, 1.0, 1.0 ) );
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 2.0f, 2.0f));
	rt3d::setUniformMatrix4fv( bumpMapProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setMaterial( bumpMapProgram, material1 );
	if ( ( frustumObj->cubeInFrustum( *cubeBumpModel, cubeBumpModel->getCenterPos() ) ) != Frustum::OUTSIDE) 
		cubeBumpModel->draw();
	mvStack.pop();

	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[3]);

	glActiveTexture(GL_TEXTURE1);
	//bind the texture to this texture unit.
	glBindTexture(GL_TEXTURE_2D, textures[4]);


	// draw a ground plane
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(20.0f, 0.2f, 20.0f));
	rt3d::setUniformMatrix4fv( bumpMapProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setMaterial( bumpMapProgram, material1 );
	cubeBumpModel->draw();
	mvStack.pop();

	////create a new cube for parallax occlusion
	glUseProgram( bumpMapTangentProgram );

	rt3d::setUniformMatrix4fv( bumpMapTangentProgram, "projection", glm::value_ptr( projection ) );
	//pass the camera position in to the shader program.
	GLuint loc = glGetUniformLocation(bumpMapTangentProgram, "cameraPos");
	glUniform3fv(loc, 1, glm::value_ptr( eye ) );
	//set the light for the current program
	rt3d::setLightPos( bumpMapTangentProgram, glm::value_ptr( tmp ) );

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textures[1]);


	mvStack.push(mvStack.top());
	cubeBumpModel->move( glm::vec3( -9.0f, 5.0f, -5.0f ) );
	mvStack.top() = glm::translate(mvStack.top(),cubeBumpModel->getCenterPos());
	mvStack.top() = glm::rotate( mvStack.top(), 45.0f, glm::vec3( 1.0, 1.0, 1.0 ) );
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(2.0f, 2.0f, 2.0f));
	rt3d::setUniformMatrix4fv( bumpMapTangentProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setMaterial( bumpMapTangentProgram, material1 );
	if ( ( frustumObj->cubeInFrustum( *cubeBumpModel, cubeBumpModel->getCenterPos() ) ) != Frustum::OUTSIDE) 
		cubeBumpModel->draw();
	mvStack.pop();


	//Creating particles and an effect.
	glUseProgram(particleShaderProgram);

	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[2]);
	//creating a counter
	timer=clock();
	timer = timer/50;
	//converting counter to GLfloat to be passed to the shader
	Timer = timer;
	GLuint particleLoc = glGetUniformLocation(particleShaderProgram, "timer");
	glUniform1f( particleLoc, Timer );
	//setting the location for the particles to be spawned.
	glm::vec3 particleEmitter(0.0f ,60.0f, 0.0f);
	particleLoc = glGetUniformLocation(particleShaderProgram, "particleEmitter");
	glUniform3fv( particleLoc, 1, glm::value_ptr( particleEmitter ) );
	rt3d::setUniformMatrix4fv(particleShaderProgram, "modelview", glm::value_ptr( mvStack.top() ) );
	rt3d::setUniformMatrix4fv(particleShaderProgram, "projection", glm::value_ptr( projection ) );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glDepthMask(0);
	//if ( ( frustumObj->cubeInFrustum( *cubeBumpModel, cubeBumpModel->getCenterPos() ) ) != Frustum::OUTSIDE) 
		ParticleCreator->Draw();
	glDepthMask(1);
	glDisable(GL_BLEND);


	// remember to use at least one pop operation per push...
	mvStack.pop();

	glDepthMask(GL_TRUE);
	SDL_GL_SwapWindow(window); // swap buffers
}

void NewGame::run()
{
	SDL_Window * hWindow; // window handle
	SDL_GLContext glContext; // OpenGL context handle
	hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		update();
		draw(hWindow); // call the draw function
	}
	ParticleCreator->~Particles();
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(hWindow);
	SDL_Quit();
}