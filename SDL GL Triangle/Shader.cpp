#include "Shader.h"


Shader::Shader( std::string _name, ShaderType _type )
{
  m_name=_name;
  m_shaderType = _type;
  m_debugState = true;
  m_compiled=false;
  switch (_type)
  {
    case VERTEX : { m_shaderHandle = glCreateShader(GL_VERTEX_SHADER_ARB); break; }
    case FRAGMENT : { m_shaderHandle = glCreateShader(GL_FRAGMENT_SHADER_ARB); break; }
    case GEOMETRY : { m_shaderHandle = glCreateShader(GL_GEOMETRY_SHADER_EXT); break; }
    case TESSELATION_CONTROL : { m_shaderHandle = glCreateShader(GL_TESS_CONTROL_SHADER); break; }
    case TESSELATION_EVALUATION : { m_shaderHandle = glCreateShader(GL_TESS_EVALUATION_SHADER); break; }
  }
  m_compiled = false;
  m_refCount = 0;
  m_source = 0;
}


Shader::~Shader(void)
{
}


void Shader::printShaderError(const GLint shader)
{
		int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(shader))
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(shader))
			glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(shader,maxLength, &logLength, logMessage);
		std::cout << "Shader Info Log:" << std::endl << logMessage << std::endl;
		delete [] logMessage;
	}
	// should additionally check for OpenGL errors here
}


//Loading the shader.
void Shader::Load( std::string _name )
{
	//See if there is a source already attatched
	if ( m_source != 0 )
	{
		std::cerr<<( "deleting existing source code\n" );
		delete m_source;
	}
	std::ifstream shaderSource( _name.c_str(  ) );
	if( !shaderSource.is_open() )
	{
		std::cerr<< " File Not Found " << _name.c_str() << "\n";
		exit( EXIT_FAILURE );
	}

	//Read the data in
	m_source = new std::string( ( std::istreambuf_iterator<char>( shaderSource ) ), std::istreambuf_iterator<char>( ) );
	shaderSource.close();
	*m_source += "\0";

	const char* data = m_source->c_str();
	glShaderSource(m_shaderHandle, 1, &data, nullptr);

	if( m_debugState == true )
	{
		std::cerr << "Shader Loaded And Source Attached";
	}
}


void Shader::Compile()
{
	if( m_source == 0 )
	{
		std::cerr << " warning no shader source loaded\n ";
		return;
	}
	glCompileShader( m_shaderHandle );

	if( m_debugState == true )
	{
		std::cerr << " Compiling Shader " << m_name.c_str() << " \n";
		printShaderError(m_shaderHandle);
	}
	m_compiled = true;
}

void Shader::incrementRefCount()
{
	m_refCount +=1;
}

GLuint Shader::getShaderHandle()
{
	return m_shaderHandle;
}

