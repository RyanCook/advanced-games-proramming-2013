#pragma once
#include<GL\glew.h>
#include<glm\glm.hpp>
#include<map>
#include<iostream>
#include<vector>

#define MESHVERTEX	0
#define COLOUR		1
#define NORMAL		2
#define TEXCOORD    3
#define INDEX		4
#define TANGENT     5
#define BITANGENT   6

class Mesh
{
public:
	Mesh(void);
	~Mesh(void);
	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
		const GLfloat* texcoords,const GLfloat* tangents, const GLfloat* bitangents, const GLuint indexCount, const GLuint* indices);

	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
		const GLfloat* texcoords);

	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices);
	GLuint createColourMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours);
	void drawMesh(const GLuint mesh, const GLuint numVerts, const GLuint primitive); 
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);
	void updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size);
	glm::vec3 FloatToVec3( std::vector<GLfloat> vec3, int counter );
	glm::vec2 FloatToVec2( std::vector<GLfloat> vec2, int counter );
	glm::vec3 Mesh::FloatToVec3Norm(std::vector<GLfloat> vec3Norm, int counter);
	void computeTangentBasis(std::vector<GLfloat> vertices, std::vector<GLfloat> texCoords, std::vector<GLfloat> normals, 
		std::vector<GLfloat> tangents, std::vector<GLfloat> bitangents, GLuint size, std::vector<GLuint> indicies );

};

