#include "Model.h"

Model::Model()
{

}

Model::~Model()
{

}

void Model::init(std::vector<GLfloat> verts, std::vector<GLfloat> norms, std::vector<GLfloat> tex_coords, std::vector<GLuint> indices)
{
	this->verts = verts;
	this->norms = norms;
	this->tex_coords = tex_coords;
	this->indices = indices;

	GLuint size = this->indices.size();

	meshIndexCount = size;

	meshObject = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());

	centerPos = glm::vec3(0.0f, 0.0f, 0.0f);
	
	radius = 1.0f;
}

void Model::draw()
{
	rt3d::drawIndexedMesh(meshObject,meshIndexCount,GL_TRIANGLES);
}

glm::vec3 Model::getVerts(int i, glm::vec3 translation)
{
	// Since the vert data is stored as GLfloats and not vec3's
	// e.g. vert[i] = vertexi.x but this function needs vert[i] = vertexi(x,y,z)
	// i needs to be ajusted to compensate for this
	// e.g. vert[i] = (verts[i], verts[i+1], verts[i+2])
	i = (i * 3);

	return glm::vec3(verts[i]+translation.x, verts[i+1]+translation.y, verts[i+2]+translation.z);
}

void Model::move(glm::vec3 newVec)
{
	centerPos = newVec;
}