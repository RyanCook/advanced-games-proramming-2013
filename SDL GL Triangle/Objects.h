#pragma once
#include"Mesh.h"
#include<GL\GL.h>
#include"rt3dObjLoader.h"
#include<vector>
#include<string>

class Objects
{
public:
	Objects(void);
	~Objects(void);
	void Draw();
	void Init(std::string x);
	GLuint getMesh();
private:
	GLuint mesh;
	Mesh* BuildMesh;
	GLuint meshIndexCount;
};

