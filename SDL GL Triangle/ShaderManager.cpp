#include "ShaderManager.h"


ShaderManager::ShaderManager(void)
{
	m_DebugState = true;
	m_NullProgram = new ShaderProgram("NULL");
}

void ShaderManager::use(std::string _name)
{
	std::map<std::string,ShaderProgram*>::const_iterator itr = m_ShaderPrograms.find(_name);
	if( itr == m_ShaderPrograms.end() ) return;

	m_ShaderPrograms[_name]->use();

}
// deleting shaders and programs
ShaderManager::~ShaderManager(void)
{
	delete m_NullProgram;

	std::map<std::string,Shader*>::const_iterator shaderItr = m_Shaders.begin();

	while( shaderItr != m_Shaders.end() )
	{
		delete shaderItr->second;
		shaderItr++;
	}
	m_Shaders.clear();

	std::map<std::string,ShaderProgram*>::const_iterator programItr= m_ShaderPrograms.begin();
	while( programItr != m_ShaderPrograms.end() )
	{
		delete programItr->second;
		programItr++;
	}
}

/*

* - Creates a new ShaderProgram

* - @param string _name is used to give the newly created shader
a name for it to be identified.

*/
void ShaderManager::createShaderProgram(std::string _name)
{
	std::cerr << "Creating empty shader program " << _name.c_str() << "\n";
	m_ShaderPrograms[ _name ] = new ShaderProgram( _name );
}

/*

* - loads the shader source into the map

* - @param string _shaderName is used to find the shader identifier, or name
as it is used to search the map.

* - @param string _sourceFile is used to load the actual shader and put it into the 
value Shader* part of the map. So now the shader name
and actual shader program are related in the map

*/
void ShaderManager::attatchShader(std::string _name, ShaderType _type)
{
	m_Shaders[_name] = new Shader(_name, _type);
}

/*

* - loads the actual shader source, i.e the .frag and .vert files into the map.

* - @param string _shaderName allows the name of the shader to be found in the map
so that the source file is able to be put into the value
part of that part of the map, allowing a relationaship to
form between the two.

* - @param string _sourceFile allows the sourcefile that is to be loaded to be specified
and then to be loaded into the value part of the map.

*/

void ShaderManager::loadShaderSource(std::string _shaderName, std::string _sourceFile)
{
	std::map<std::string, Shader*>::const_iterator shader = m_Shaders.find( _shaderName );
	//make sure there is a valid shader and program
	if( shader != m_Shaders.end() )
	{
		shader->second->Load(_sourceFile);
	}
	else{std::cerr << "Warning shader not known in loadShaderSource" << _shaderName.c_str();}
}

/*
* - Compiles the actual shader

* - @param string _name is used to identify the shader which is to be
compiled by using this input to search the map

*/
void ShaderManager::compileShader(std::string _name)
{
	//search the map for specified shader
	std::map <std::string, Shader* >::const_iterator shader = m_Shaders.find( _name );
	// compile the shader
	if( shader != m_Shaders.end( ) )
	{
		shader->second->Compile();
	}
	// or if the shader cannot be found print out this message
	else{std::cerr << "Warning shader not known in compile " << _name.c_str();}
}

void ShaderManager::bindAttribute( std::string _program, GLuint index, std::string _attribName )
{
		//search the map for specified shader
	std::map <std::string, ShaderProgram* >::const_iterator program = m_ShaderPrograms.find( _program );
	// compile the shader
	if( program != m_ShaderPrograms.end( ) )
	{
		program->second->BindAttrib( index, _attribName );
	}
	// or if the shader cannot be found print out this message
	else{std::cerr << "Warning shader not known in compile " << _program.c_str();}
};

/*

* - Attatches a shader to a shader program.

* - @param string _Program is used to find the name of the program which 
the shader will be attatched to.

* - @param string _Shader is used to find the shader that will be attatched to the program

*/
void ShaderManager::attatchShaderToProgram(std::string _Program, std::string _Shader)
{
	//get an iterator to the shader and the program
	std::map <std::string, Shader* >::const_iterator shader = m_Shaders.find( _Shader );
	std::map <std::string, ShaderProgram* >::const_iterator program = m_ShaderPrograms.find( _Program );
	//make sur the shader and program both acutally exist
	if( shader != m_Shaders.end() && program != m_ShaderPrograms.end( ) )
	{
		//attatch the shader program. class ShaderPrograms AttachShader function takes a Shader* 
		program->second->AttatchShader(shader->second);
		//increment the shader ref count so we know how many references the one program has.
		shader->second->incrementRefCount();

		if( m_DebugState == true )
		{
			std::cerr << _Shader.c_str() << " attatched to program " << _Program.c_str() << "\n";
		}
		else{std::cerr << "Warning cant attach " << _Shader.c_str() << " to program "<< _Program.c_str() << "\n";}
	}
}

/*
* - overloads the [] operator to take a program/std::string

* - @param string _ProgramName allows the program to be found in the map
*/

ShaderProgram* ShaderManager::operator[](const std::string &_ProgramName)
{
	//iterates through the map in order to find the program name that was passed in
	std::map<std::string, ShaderProgram* >::const_iterator program = m_ShaderPrograms.find(_ProgramName);
	//make sure the program exists
	if( program != m_ShaderPrograms.end() )
	{
		return program->second;
	}
	else
	{
		std::cerr<<"Warning Program not know in [] "<<_ProgramName.c_str();
		std::cerr<<"returning a null program and hoping for the best\n";
		return m_NullProgram;
	}


}


/*
* - Iterates through the Shader Program map and calls the link function on the shader program 
that is specified. The link function that is called belongs to the shader program class

* - @param string _name is used to find the program in the map that the link function has to be called on.

*/
void ShaderManager::linkProgramObject(std::string _name)
{
	std::map<std::string, ShaderProgram* >::const_iterator program = m_ShaderPrograms.find(_name);

	if( program != m_ShaderPrograms.end() )
	{
		program->second->link();
	}
	else{std::cerr << "Program " <<_name <<" could not be found in shader manager link program obj" << " /n " ;}
}
/*

* - Iterates through the Shader Program map and calls the getProgramID function on the shader program 
that is specified. The getProgramID function that is called belongs to the shader program class

* - @param string _name is used to find the program in the map that the getProgramID function has to be called on.

*/
GLuint ShaderManager::getProgramID(std::string _name)
{
	std::map<std::string, ShaderProgram* >::const_iterator program = m_ShaderPrograms.find(_name);

	//check the program exists
	if( program != m_ShaderPrograms.end( ) )
	{
		return program->second->getID();
	}
	else{std::cerr << "Program " <<_name <<" could not be found in shader manager getProgramID" << " /n " ;}
}

void ShaderManager::initVertShaderProgram(const std::string name,const std::string vName,const std::string fName,const std::string vPath,const std::string fPath)
{

	createShaderProgram(name);
	attatchShader(vName,VERTEX);
	attatchShader(fName,FRAGMENT);

	loadShaderSource(vName,vPath);
	loadShaderSource(fName,fPath);

	compileShader(vName);
	compileShader(fName);

	attatchShaderToProgram(name,vName);
	attatchShaderToProgram(name,fName);

	linkProgramObject(name);	
}

void ShaderManager::setLight(std::string _program, const lightStruct light)
{
		auto program = m_ShaderPrograms.find(_program);

	//check the program exists
	if( program != m_ShaderPrograms.end( ) )
	{
		program->second->setUniform4fv("light.ambient", 1, light.ambient);
		program->second->setUniform4fv("light.diffuse", 1, light.diffuse);
		program->second->setUniform4fv("light.specular", 1, light.specular);
		program->second->setUniform4fv("lightPosition", 1, light.position);
	}
	else{std::cerr << "Program " <<_program <<" could not be found in shader manager getProgramID" << " /n " ;}

}

void ShaderManager::setMaterial(std::string _program, const materialStruct material)
{
			auto program = m_ShaderPrograms.find(_program);

	//check the program exists
	if( program != m_ShaderPrograms.end( ) )
	{
		program->second->setUniform4fv("material.ambient", 1, material.ambient);
		program->second->setUniform4fv("material.diffuse", 1, material.diffuse);
		program->second->setUniform4fv("material.specular", 1, material.specular);
		program->second->setUniform1f("material.shininess", material.shininess);
	}
	else{std::cerr << "Program " <<_program <<" could not be found in shader manager getProgramID" << " /n " ;}
}



//GLuint ShaderManager::getUniformLocation(std::string _program, std::string _uniform)
//{
//	std::map<std::string, ShaderProgram* >::const_iterator program = m_ShaderPrograms.find(_program);
//
//	//check the program exists
//	if( program != m_ShaderPrograms.end( ) )
//	{
//		program->second->getUniformLocation( _uniform );
//	}
//}
//