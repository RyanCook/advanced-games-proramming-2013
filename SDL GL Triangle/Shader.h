#pragma once
#include<iostream>
#include<fstream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include<string>

enum ShaderType{
	VERTEX,
	FRAGMENT,
	GEOMETRY,
	TESSELATION_CONTROL,
	TESSELATION_EVALUATION
};

class Shader
{
public:
	Shader( std::string _name, ShaderType _type );
	~Shader(void);
	void Compile();
	void Load(std::string _name);
	void ToggleDebug();
	GLuint getShaderHandle();
	void incrementRefCount();
	void decrementRefCount();
	int getRefCount();
	void printShaderError(const GLint shader);
private:

	std::string m_name;
	std::string* m_source;
	bool m_compiled;
	ShaderType m_shaderType;
	GLuint m_shaderHandle;
	bool m_debugState;
	int m_refCount;

};

