#ifndef MODEL_H
#define MODEL_H

#include<glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stack>

#include "rt3d.h"
#include "rt3dObjLoader.h"

class Model {

public:
	Model();
	~Model();

	// Intialise
	void init(std::vector<GLfloat> verts, std::vector<GLfloat> norms, std::vector<GLfloat> tex_coords, std::vector<GLuint> indices);

	// Draw
	void draw();

	glm::vec3 getVerts(int i, glm::vec3 translation);

	GLuint getSize() { return ( verts.size() / 3 ); }

	void move(glm::vec3 newVec);

	glm::vec3 getCenterPos() { return centerPos; }

private:
	glm::vec3 centerPos;
	GLfloat radius;
	
	GLuint meshObject;

	GLuint meshIndexCount;

	std::vector<GLfloat> verts;
	std::vector<GLfloat> norms;
	std::vector<GLfloat> tex_coords;
	std::vector<GLfloat> tex_coords1;
	std::vector<GLuint> indices;

};

#endif