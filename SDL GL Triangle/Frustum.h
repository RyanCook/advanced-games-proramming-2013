#ifndef FRUSTUM_H
#define FRUSTUM_H

#include <glm/glm.hpp>
#include <math.h>

#include "Plane.h"

#define DEG_TO_RADIAN 0.017453293

class Frustum{

public:
	Frustum();
	~Frustum();

	void setFrustrum(float fov, float ratio, float nearClip, float farClip);
	void setCamera(glm::vec3 eye, glm::vec3 at, glm::vec3 up);

	static enum {OUTSIDE, INTERSECT, INSIDE};

    int cubeInFrustum(Model &model, glm::vec3 translation);
	int sphereInFrustum(glm::vec3 pos, float radius);

	glm::vec3 nearCenter, nearTopLeft, nearBottomLeft, nearTopRight, nearBottomRight;
	glm::vec3 farCenter, farTopLeft, farBottomLeft, farTopRight, farBottomRight;

	Plane planes[6];

private:
	float fov, ratio;
	//Plane planes[6];

	float nearHeight, nearWidth, nearDistance;
	
	float farHeight, farWidth, farDistance;
	
	enum { TOP = 0, BOTTOM, LEFT, RIGHT, NEARP, FARP };
	
	

};

#endif