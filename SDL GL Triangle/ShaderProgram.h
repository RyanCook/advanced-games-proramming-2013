#pragma once
#include<iostream>
#include<vector>
#include"Shader.h"
#include<ostream>
#include<map>
#include<GL\GL.h>
#include<string>

//struct lightStruct {
//	GLfloat ambient[4];
//	GLfloat diffuse[4];
//	GLfloat specular[4];
//	GLfloat position[4];
//};
//
//struct materialStruct {
//	GLfloat ambient[4];
//	GLfloat diffuse[4];
//	GLfloat specular[4];
//	const GLfloat shininess;
//};

class ShaderProgram
{
public:
	ShaderProgram(std::string _name);
	~ShaderProgram(void);
	void use();
	void AttatchShader(Shader* _shader);
	void BindAttrib(GLuint index, std::string _AttribName);
	void link();
	GLuint getID();
	GLuint getUniformLocation(std::string _name) const;
	void setUniform1f(std::string _varName, float v0) const;
	void setUniform2fv(const std::string &str,int count,float *value) const;
	void setUniform3fv(const std::string &str,int count,float *value) const;
	void setUniform4fv(const std::string &str,int count,const GLfloat *value) const;
	void setUniformMatrix3fv(const std::string str,int count,bool t,float * value);
	void setUniformMatrix4fv(const std::string str,int count,bool t,float * value);
	void getUniformfv(std::string _name, float* o_Values) const;
	bool vertexAttribPointer(std::string _name, GLint _size, GLenum _type, GLsizei _stride, const GLvoid* _data, bool _normalise) const;
	void vertexAttrib1f(std::string _name, GLfloat _v0) const ;
	void setLightPos(const std::string _name, GLfloat* data);
	//void setLight(const lightStruct light);
	//void setMaterial(const materialStruct material);
private:
	bool m_Linked;
	bool m_DebugState;
	bool m_Active;
	GLuint m_ProgramID;
	std::string m_ProgramName;
	std::vector<Shader*> m_Shaders;
	std::map<std::string, GLuint> m_Attribs;
};

