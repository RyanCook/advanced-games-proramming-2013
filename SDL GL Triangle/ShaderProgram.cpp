#include "ShaderProgram.h"


ShaderProgram::ShaderProgram(std::string _name)
{
	std::string NULLSTRING = "NULL";
	//Creating a NULL program so that the shader manager can then return a null object.
	if(_name != NULLSTRING)
	{
		m_ProgramID = glCreateProgram();
	}
	else
	{
		m_ProgramID = 0;
	}
	std::cerr << "Created Program ID is " << m_ProgramID << "\n";
	m_DebugState = true;
	m_ProgramName = _name;
	m_Linked = false;
	m_Active = false;
}


ShaderProgram::~ShaderProgram(void)
{
	glDeleteProgram(m_ProgramID);
}

//attach a shader to a shader program
void ShaderProgram::AttatchShader( Shader* _shader )
{
	m_Shaders.push_back(_shader);
	glAttachShader(m_ProgramID, _shader->getShaderHandle() );
}
/*
* - sets the current program to be active and sets a boolean to true.
*/
void ShaderProgram::use()
{
	if( m_Active ) return;
	glUseProgram( m_ProgramID );
	m_Active = true;
}

//linking and error reporting

void ShaderProgram::link()
{
	glLinkProgram(m_ProgramID);
	if(m_DebugState == true)
	{
		std::cerr << " linking shader " << m_ProgramName.c_str() << "\n";
	}
	GLint infoLogLength = 0;
	/*
	//params(The Last value) returns the number of characters in the information log for
	//program including the null termination character (i.e., the size of the
	//character buffer required to store the information
	//log). If program has no information log, a value of 0 is returned.
	//Basically puts the size of the char buffer reqd to store the info log into the parameter "infoLogLength"
	*/
	glGetProgramiv(m_ProgramID, GL_INFO_LOG_LENGTH, &infoLogLength);

	std::cerr << "Link Log Length" << infoLogLength << "\n";

	if( infoLogLength > 0 )
	{
		char* infoLog = new char[infoLogLength];
		GLint charsWritten = 0;

		//returns the actual information log for the program
		glGetProgramInfoLog(m_ProgramID, infoLogLength, &charsWritten, infoLog);

		std::cerr << infoLog << std::endl;
		delete [] infoLog;
		glGetProgramiv(m_ProgramID, GL_LINK_STATUS, &infoLogLength);

		if( infoLogLength == GL_FALSE )
		{
			std::cerr << "Program link failed exiting \n";
			exit(EXIT_FAILURE);
		}
	}
	m_Linked = true;
}


/*

Binds Attributes that are passed into the function to a specific shader program

@param GLuint _index - Takes a value in and uses it to index the attrib 
value passed in by use of a map

@param std::string _attribName - Takes a String in and puts it into the map,
using the map to index the attrib.

*/
void ShaderProgram::BindAttrib(GLuint _index, std::string _attribName)
{
	if(m_Linked == true)
	{
		std::cerr << "Warning, binding attrib after link\n";
	}

	m_Attribs[_attribName] = _index;
	glBindAttribLocation( m_ProgramID, _index, _attribName.c_str() );
	std::cerr << "bindAttribLoc " << m_ProgramID << " index " << _index << "name" << _attribName.c_str() << std::endl;

}

/*

* - function allows access to the previously bound attributes. It does this by taking in values to be passed 
into the glVertexAttribPointer function.

* - @param char* _name is the name of the attribute to be passed in that is being looked for

* - @param GLint _size is used to specify the number of components per generic vertex attribute
i.e colour, position. Must be an integer.

* - @param GLenum _type specifies the data type of each componenet in the array. Since the type specified is
a GLenum, it can take a number of the OpenGL enum's including GLuint, GLint and more.

* - @param GLsizei _stride specifies the byte offset between consecutive generic vertex attribs, if it is 0,
the attribs are understood to be packed closely together, initial value is 0.

* - @param bool_normalise specifies whether fixed point data values should be normalized or not.

* - @param _data specifies an offset to the first generic vertex attrib in the array

Can search the glVertexAttribPointer function for more info.
*/

bool ShaderProgram::vertexAttribPointer( std::string _name, GLint _size, GLenum _type, GLsizei _stride, const GLvoid* _data, bool _normalise ) const

{
	std::map< std::string, GLuint >::const_iterator attrib = m_Attribs.find( _name );
	//Make sure the program we have is valid

	if( attrib != m_Attribs.end() )
	{
		glVertexAttribPointer(attrib->second,_size,_type,_normalise,_stride,_data);
		return true;
	}
	else
	{
		return false;
	}
}
/*
* - Takes in the name of the attribute that is to be searched for and uses this to find the attrib
then uses the passed in float to set the value in the map. This is what iterator(which is attrib)
->second refers to, the value of the map which holds a string and a float

* - @param char* _name is used to search the map in order to find the attribute.

* - @param GLfloat _v0 is a float that is used to set/change the value belonging to the aforementioned attribute.
*/

void ShaderProgram::vertexAttrib1f( std::string _name, GLfloat _v0 ) const
{
	std::map<std::string, GLuint>::const_iterator attrib = m_Attribs.find(_name);
	//make sure we have a valid program.
	if( attrib != m_Attribs.end() )
	{
		glVertexAttrib1f(attrib->second, _v0);
	}
}

/*
* - Uses the passed in char pointer to search for the name of the 
uniform that belongs to the shader program. Then puts an integer
value into a GLint value, which is then returned at the end of the 
function. When using this function it should be written
GLuint someValue = ShaderProgram::getUniformLocation(const char* _name);

* - @param char* _name is used to locate a uniform inside the shader program
So, the EXACT name of the unifrom must be written in
this variable.
*/

GLuint ShaderProgram::getUniformLocation(std::string _name) const
{
	GLint loc = glGetUniformLocation( m_ProgramID, _name.c_str() );
	if( loc == -1 )
	{
		std::cerr << "Uniform \"" << _name.c_str() << "\" not found in program \"" <<m_ProgramName.c_str() << "\"\n";
	}
	return loc;
}

/*

* - This function takes in a char* and a float value and uses them to set the
value of a uniform inside a shader program

* - @param char* _VarName is used to search for the uniform in the shader

* - @param float _v0 is the value that the uniform is to be set to.

*/

void ShaderProgram::setUniform1f(std::string _VarName, float _v0) const
{
	glUniform1f( getUniformLocation( _VarName ), _v0 );
}

/*
* - This gets a value for a uniform and puts it into the float* that is passed in

* - @param char* _name is used to determine the uniform location inside the 
specified program

* - @param float* o_Values is used to get the float value that the uniform is set to.

*/


void ShaderProgram::getUniformfv(std::string _name, float* o_Values) const
{
	glGetUniformfv( m_ProgramID, getUniformLocation( _name ) , o_Values );
}

/*

 * - Function looks for a uniform and replaces/sets the value with the value that is passed in

 * - @param string reference str is used to find the location of the uniform

 * - @param int count specifies the number of elements to be changed. 1 if uniform is not an array
                      or more than 1 if the uniform variable is an array.

 * - @param float* value is the value that the variable has to be set to.

*/
void ShaderProgram::setUniform2fv(const std::string &str,int count,float *value) const
{
	glUniform2fv( getUniformLocation( str.c_str() ), count, value  );
}

//Refer to comments for setUnifrom2fv as same parameters used and same thing being done with each param
void ShaderProgram::setUniform3fv(const std::string &str,int count,float *value) const
{
	glUniform3fv( getUniformLocation( str.c_str() ), count, value  );
}

//Refer to comments for setUnifrom2fv as same parameters used and same thing being done with each param
void ShaderProgram::setUniform4fv(const std::string &str,int count,const GLfloat *value) const
{
	glUniform4fv( getUniformLocation( str.c_str() ), count, value  );
}

/*

 * - Function looks for a uniform and replaces/sets the value with the value that is passed in

 * - @param string reference str is used to find the location of the uniform

 * - @param int count specifies the number of elements to be changed. 1 if uniform is not an array
                      or more than 1 if the uniform variable is an array.

 * - @param bool trans specifies whether or not to transpose the matrix  as the values are loaded
						in to the uniform.

 * - @param float* value is the value that the variable has to be set to.

*/
void ShaderProgram::setUniformMatrix3fv(const std::string str,int count,bool trans,float * value)
{
	glUniformMatrix3fv(getUniformLocation(str.c_str()),count,trans,value);
}

//refer to comments above for this function
void  ShaderProgram::setUniformMatrix4fv(const std::string str,int count,bool trans,float* value)
{
	glUniformMatrix4fv(getUniformLocation(str.c_str()),count,trans,value);
}

/*
* - Returns the shader program ID

* - @param GLuint IdOut is taken in to the function so that it can be made
                        to equal the program ID and can then be used outwith
                        this class.
*/

GLuint ShaderProgram::getID()
{
	return m_ProgramID;
}

/*
 * - takes input for the light pos and sets it as a 4x4 matrix by calling one of the methods already built into this code.

 * - @param string _uniformName is the name of the uniform that is to be set.
 
 * - @param GLfloat* data is the data that the uniform is to be set to. In this case it is the lightPos
*/
void ShaderProgram::setLightPos(const std::string _uniformName,GLfloat* data)
{
	setUniformMatrix4fv(_uniformName, 1, GL_FALSE, data);
}

/*
 * - Sets all the variables inside the lightStruct to equal that of a struct that is passed into the function.

 * - @param string _programName
*/
//void ShaderProgram::setLight(const lightStruct light)
//{
//	setUniform4fv("light.ambient", 1, light.ambient);
//	setUniform4fv("light.diffuse", 1, light.diffuse);
//	setUniform4fv("light.specualr", 1, light.specular);
//	setUniform4fv("lightPosition", 1, light.position);
//}
//void ShaderProgram::setMaterial(const materialStruct material)
//{
//	setUniform4fv("material.ambient", 1, material.ambient);
//	setUniform4fv("material.diffuse", 1, material.diffuse);
//	setUniform4fv("material.specular", 1, material.specular);
//	setUniform1f("material.shininess", material.shininess);
//}