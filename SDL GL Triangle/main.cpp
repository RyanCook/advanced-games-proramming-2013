//// MD2 animation renderer
//// This demo will load and render an animated MD2 model, an OBJ model and a skybox
//// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
//// utility library, to make creation and display of mesh objects as simple as possible
//
// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif
////
#include"NewGame.h"
//#include"ShaderInitialiser.h"

int main(int argc, char *argv[]) 
{
	NewGame *Game = new NewGame();
	Game->run();
	delete Game;
	return 0;
}