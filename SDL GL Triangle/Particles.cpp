#include "Particles.h"




/*
* - This function take an input for the number of particles and then
sets it to the int called numberOfParticles that is declared 
privately in Particles.h. The : numberOfParticles( n ) part is
because n is declared as const then numberOfParticles cannot be made
to equal it in a normal way

* - @param const int n is the number of particles that is to be passed in.

*/
Particles::Particles(const int n) : numberOfParticles( n )
{
	//traps invalid numbers.
	GLfloat timeCounter = 0.0f;
	if (numberOfParticles <=0) return;
	//creates 3 arrays of type GLfloat and sets them to the size of 
	//the number of particles which is passed into this constructor.
	//positions = new GLfloat[numberOfParticles*3];
	colours = new GLfloat[numberOfParticles*3];
	velocity = new GLfloat[numberOfParticles*3];
	startTime = new GLfloat[numberOfParticles];

	//initialise with random values
	std::srand( std::time(0) );
	int j = 0;
	for(int i=0; i<numberOfParticles; i++)
	{

		startTime[i] = timeCounter;
		timeCounter += 0.1f;
		colours[j+0] = 1.0f;
		colours[j+1] = 1.0f;
		colours[j+2] = 1.0f;
		velocity[j+0] = ( (std::rand()% 100)-50 ) / 1000.0f;
		velocity[j+1] = -( std::rand()%50 ) / 1000.0f;
		velocity[j+2] = ( (std::rand()% 100)-50 ) / 1000.0f;
		j+=3;
	}

	//an array to store each Vertex Buffer Object.
	pMeshBuffers = new GLuint[4];

}


Particles::~Particles(void)
{
	//[] positions;
	delete[] colours;
	delete[] velocity;
	delete[] pMeshBuffers;
	delete[] startTime;
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &VAO);
}


void Particles::Update()
{
}

void Particles::Init(GLuint shaderProgram)
{
	// generate and set up a Vertex Array Object for the mesh
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);


	glGenBuffers(1, &VBO);

	// VBO for vertex data
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	////VBO to take dynamic data for particle positions 
	//if (positions != nullptr) {
	//	glGenBuffers(1, &VBO);
	//	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//	glBufferData(GL_ARRAY_BUFFER, 3*numberOfParticles*sizeof(GLfloat), positions, GL_STATIC_DRAW);
	//	glVertexAttribPointer((GLuint)PARTICLEPOSITION, 3, GL_FLOAT, GL_FALSE, 0, 0);
	//	glEnableVertexAttribArray(PARTICLEPOSITION);
	//	pMeshBuffers[PARTICLEPOSITION] = VBO;
	//}

	//VBO to take dynamic data for particle colours 
	if (colours != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numberOfParticles*sizeof(GLfloat), colours, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)PARTICLECOLOUR, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(PARTICLECOLOUR);
		pMeshBuffers[PARTICLECOLOUR] = VBO;
	}

	//VBO to take dynamic data for particle velocity 
	if (velocity != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numberOfParticles*sizeof(GLfloat), velocity, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)PARTICLEVELOCITY, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(PARTICLEVELOCITY);
		pMeshBuffers[PARTICLEVELOCITY] = VBO;
	}

		//VBO to take dynamic data for particle velocity 
	if (startTime != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, numberOfParticles*sizeof(GLfloat), startTime, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)STARTTIME, 1, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(STARTTIME);
		pMeshBuffers[STARTTIME] = VBO;
	}

	//glBindAttribLocation(shaderProgram, PARTICLEPOSITION, "in_particlePosition");
	//glBindAttribLocation(shaderProgram, PARTICLECOLOUR, "in_particleColour");
	//glBindAttribLocation(shaderProgram, PARTICLEVELOCITY, "in_particleVelocity");
	//glBindAttribLocation(shaderProgram, STARTTIME, "in_startTime");

	glPointSize(8);
	
	//glDrawArrays(GL_POINTS, 0, numberOfParticles);


}

void Particles::Draw()
{
	glBindVertexArray(VAO);
	glDrawArrays(GL_POINTS, 0, numberOfParticles);
	glBindVertexArray(0);
}